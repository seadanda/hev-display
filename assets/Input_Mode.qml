import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    id: inputMode

    ColumnLayout {
        spacing: 20
        RadioButton {
            id: modePCAC
            Layout.fillWidth: true
            height: Style.btnHeight
            text: "PC-A/C"
            contentItem: Text {
                text: modePCAC.text
                color: "white"
                leftPadding: modePCAC.indicator.width + modePCAC.spacing
                verticalAlignment: Text.AlignVCenter
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
            }
        }
        RadioButton {
            id: modePRVC
            checked: true
            Layout.fillWidth: true
            height: Style.btnHeight
            text: "PC-A/C-PRVC"
            contentItem: Text {
                text: modePRVC.text
                color: "white"
                leftPadding: modePRVC.indicator.width + modePRVC.spacing
                verticalAlignment: Text.AlignVCenter
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
            }
        }
        RadioButton {
            id: modePCSV
            Layout.fillWidth: true
            height: Style.btnHeight
            text: "PC-PSV"
            contentItem: Text {
                text: modePCSV.text
                color: "white"
                leftPadding: modePCSV.indicator.width + modePCSV.spacing
                verticalAlignment: Text.AlignVCenter
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
            }
        }
        RadioButton {
            id: modeCPAP
            Layout.fillWidth: true
            height: Style.btnHeight
            text: "CPAP"
            font.family: Style.fontFamily
            font.weight: Font.Bold
            font.pointSize: Style.fontPointSize
            contentItem: Text {
                text: modeCPAP.text
                color: "white"
                leftPadding: modeCPAP.indicator.width + modeCPAP.spacing
                verticalAlignment: Text.AlignVCenter
                font.family: Style.fontFamily
                font.weight: Font.Bold
                font.pointSize: Style.fontPointSize
            }
        }
        RowLayout {
            Layout.alignment: Qt.AlignHCenter
            Layout.fillWidth: true
            Layout.topMargin: 10
            spacing: 10
            RoundButton {
                Layout.fillWidth: true
                Layout.minimumWidth: Style.btnHeight * 1.5
                Layout.minimumHeight: Style.btnHeight
                Image {
                    anchors.centerIn: parent
                    source: "svg/check-solid.svg"
                    sourceSize.height: parent.height * 0.8
                }
            }
            RoundButton {
                Layout.fillWidth: true
                Layout.minimumWidth: Style.btnHeight * 1.5
                Layout.minimumHeight: Style.btnHeight
                Image {
                    anchors.centerIn: parent
                    source: "svg/times-solid.svg"
                    sourceSize.height: parent.height * 0.8
                }
            }
        }
    }
}
