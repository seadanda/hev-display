import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Item {
    Rectangle {
        color: Style.btnDnColor
        anchors.fill: parent
    }

    ColumnLayout {
        width: parent.width
        height: parent.height
        GridLayout {
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.margins: 5

            columns: 4

            Ctrl {
                id: ctrl1
                title: "Setting1"
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
            Ctrl {
                title: "Setting2"
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
            Ctrl {
                title: "Setting3"
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
            Ctrl {
                title: "Setting4"
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
            Ctrl {
                title: "Setting5"
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
            Ctrl {
                title: "Setting6"
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
        }
        RowLayout {
            spacing: height
            Layout.minimumHeight: parent.height * 0.1
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: true
            Layout.margins: 5

            RoundButton {
                Layout.minimumHeight: parent.height
                Layout.minimumWidth: height * 2
                Image {
                    anchors.centerIn: parent
                    source: "svg/check-solid.svg"
                    sourceSize.width: parent.height * 0.8
                    sourceSize.height: parent.height * 0.8
                }
            }
            RoundButton {
                Layout.minimumHeight: parent.height
                Layout.minimumWidth: height * 2
                Image {
                    anchors.centerIn: parent
                    source: "svg/times-solid.svg"
                    sourceSize.width: parent.height * 0.8
                    sourceSize.height: parent.height * 0.8
                }
            }
        }
    }
}
