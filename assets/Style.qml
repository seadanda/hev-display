pragma Singleton
import QtQml 2.12
import QtQuick.Window 2.12

QtObject {
    property string fontFamily: "Open Sans"
    property int fontPixelSize: Math.ceil(btnHeight * 0.8)
    property int fontPointSize: 20
    property color mainBgColor: "black" //"#96a6b5"
    property color topBgColor: "#525556"
    property color cellBgColor: "#525556"
    property color btnUpColor: "#e0e0e0"
    property color btnDnColor: "#aeaeae"
    property int btnWidth: Math.ceil(Screen.pixelDensity * 15) //15mm
    property int btnHeight: Math.ceil(Screen.pixelDensity * 15) //15mm
    property int btnRadius: 10
    property int holdSecs: 1

    property color pressureAirSupplyColor: "#8b2be2"
    property color pressureAirRegulatedColor: "#e8000b"
    property color pressureO2SupplyColor: "#023eff"
    property color pressureO2RegulatedColor: "#00d7ff"
    property color pressureBufferColor: "#ffc400"
    property color pressureInhaleColor: "#9f4800"
    property color pressurePatientColor: "#f14cc1"
    property color flowDiffPatientColor: "#ff7c00"
    property color volumeColor: "#1ac938"
}
