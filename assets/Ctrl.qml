import QtQuick 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12

Item {
    property string title: "Title"

    RowLayout {
        height: parent.height
        width: parent.width

        Cell {
            id: cell
            title: parent.parent.title
            Layout.fillHeight: true
            Layout.fillWidth: true
        }
        Button {
            id: editBtn
            Layout.fillHeight: true
            Layout.fillWidth: true
            text: "click me to edit"
        }
    }
}
