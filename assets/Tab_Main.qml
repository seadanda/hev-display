import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtCharts 2.3

Item {
    property bool antialias: true
    property bool openGL: false
    property int axisTimeMin : -15

    Rectangle {
        color: Style.btnDnColor
        anchors.fill: parent
    }
    RowLayout {
        anchors.fill: parent

        ColumnLayout {
            id: mainTimeCharts
            Layout.fillHeight: true
            Layout.fillWidth: true
            spacing: -15

            ChartView {
                id: mainPressureChart
                objectName: "mainPressureChart"
                Layout.fillWidth: true
                Layout.fillHeight: true

                legend.visible: false
                animationOptions: ChartView.NoAnimation
                antialiasing: antialias
                theme: ChartView.ChartThemeDark

                ValueAxis {
                    id: pressureAxisTime
                    min: axisTimeMin
                    max: 0
                    tickCount: Math.abs(max-min) / 5 + 1
                    labelFormat: "%.1f"
                }
                ValueAxis {
                    id: pressureAxisCmH2O
                    objectName: "pressureAxisCmH2O"
                    titleText: "Pressure [cmH2O]"
                    labelFormat: "%.1f"
                    min: 0
                    max: 30
                }
                LineSeries {
                    objectName: "main_pressure"
                    axisX: pressureAxisTime
                    axisY: pressureAxisCmH2O
                    useOpenGL: openGL
                }
            }

            ChartView {
                id: flowChart
                objectName: "mainFlowChart"
                Layout.fillWidth: true
                Layout.fillHeight: true

                legend.visible: false
                animationOptions: ChartView.NoAnimation
                antialiasing: antialias
                theme: ChartView.ChartThemeDark

                ValueAxis {
                    id: flowAxisTime
                    min: axisTimeMin
                    max: 0
                    tickCount: Math.abs(max-min) / 5 + 1
                    labelFormat: "%.1f"
                }
                ValueAxis {
                    id: flowAxisMlMin
                    titleText: "Flow [mL/min]"
                    objectName: "flowAxisMlMin"
                    labelFormat: "%.1f"
                    min: -1000
                    max: 1500
                }
                LineSeries {
                    objectName: "main_flow"
                    axisX: flowAxisTime
                    axisY: flowAxisMlMin
                    useOpenGL: openGL
                }
            }

            ChartView {
                id: mainVolumeChart
                objectName: "mainVolumeChart"
                Layout.fillWidth: true
                Layout.fillHeight: true

                legend.visible: false
                animationOptions: ChartView.NoAnimation
                antialiasing: antialias
                theme: ChartView.ChartThemeDark

                ValueAxis {
                    id: volumeAxisTime
                    min: axisTimeMin
                    max: 0
                    tickCount: Math.abs(max-min) / 5 + 1
                    labelFormat: "%.1f"
                }
                ValueAxis {
                    id: volumeAxisMl
                    objectName: "volumeAxisMl"
                    titleText: "Volume [mL]"
                    labelFormat: "%.1f"
                    min: 0
                    max: 300
                }
                LineSeries {
                    objectName: "main_volume"
                    axisX: volumeAxisTime
                    axisY: volumeAxisMl
                    useOpenGL: openGL
                }
            }
        }
        Grid {
            id: grid
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignVCenter
            columns: 2
            spacing: 10
            topPadding: 10

            Repeater {
                model: ListModel {}
                Component.onCompleted: {
                    model.append({
                        name: "FIO<sub>2</sub>",
                        unit: "%",
                        value: "62",
                        color: "black"
                    })
                    model.append({
                        name: "I:E",
                        unit: "",
                        value: "1.3",
                        color: "black"
                    })
                    model.append({
                        name: "P<sub>PEAK</sub>",
                        unit: "cmH20",
                        value: "20.0",
                        color: "black"
                    })
                    model.append({
                        name: "P<sub>PLATEAU</sub>",
                        unit: "cmH20",
                        value: "4.0",
                        color: "black"
                    })
                    model.append({
                        name: "P<sub>MEAN</sub>",
                        unit: "cmH20",
                        value: "13.0",
                        color: "black"
                    })
                    model.append({
                        name: "P<sub>EEP</sub>",
                        unit: "cmH20",
                        value: "5",
                        color: "black"
                    })
                    model.append({
                        name: "VTI",
                        unit: "mL",
                        value: "285",
                        color: "black"
                    })
                    model.append({
                        name: "VTE",
                        unit: "mL",
                        value: "273",
                        color: "black"
                    })
                    model.append({
                        name: "MVI",
                        unit: "L/min",
                        value: "6.5",
                        color: "black"
                    })
                    model.append({
                        name: "MVE",
                        unit: "L/min",
                        value: "6.0",
                        color: "black"
                    })
                }
                delegate: Rectangle {
                    width: applicationWindow.width / 7
                    height: applicationWindow.height / 7.5
                    border.color: color
                    border.width: 3
                    radius: 10
                    color: Style.btnUpColor
                    Label {
                        id: label
                        text: name
                        font.family: Style.fontFamily
                        font.pointSize: 12
                        font.weight: Font.Bold
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                    Text {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.bottom: currentValueRect.top
                        text: unit
                        font.family: Style.fontFamily
                        font.pixelSize: 15
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignTop
                        width: parent.width
                    }
                    Rectangle {
                        id: currentValueRect
                        color: "black"
                        width: parent.width
                        height: applicationWindow.height / 15
                        radius: 10
                        anchors.bottom: parent.bottom
                        Text {
                            id: currentValue
                            text: value
                            font.family: Style.fontFamily
                            font.pixelSize: 40
                            anchors.verticalCenter: parent.verticalCenter
                            horizontalAlignment: Text.AlignHCenter
                            anchors.left: parent.left
                            anchors.right: parent.right
                            color: "white"
                        }
                    }
                }
            }
            RowLayout {
                width: applicationWindow.width / 7

                RoundButton {
                    text: "60s"
                    font.family: Style.fontFamily
                    font.weight: Font.Bold
                    font.pixelSize: 30
                    radius: 10
                    Layout.fillWidth: true
                    onClicked: axisTimeMin = -60
                }
                RoundButton {
                    text: "30s"
                    font.family: Style.fontFamily
                    font.weight: Font.Bold
                    font.pixelSize: 30
                    radius: 10
                    Layout.fillWidth: true
                    onClicked: axisTimeMin = -30
                }
            }
            RowLayout {
                width: applicationWindow.width / 7

                RoundButton {
                    text: "15s"
                    font.family: Style.fontFamily
                    font.weight: Font.Bold
                    font.pixelSize: 30
                    radius: 10
                    Layout.fillWidth: true
                    onClicked: axisTimeMin = -15
                }
                RoundButton {
                    text: "5s"
                    font.family: Style.fontFamily
                    font.weight: Font.Bold
                    font.pixelSize: 30
                    radius: 10
                    Layout.fillWidth: true
                    onClicked: axisTimeMin = -5
                }
            }
        }
        ColumnLayout {
            id: mainLoopCharts
            spacing: -15

            ChartView {
                id: loopPressureVolumeChart
                objectName: "loopPressureVolumeChart"
                Layout.fillWidth: true
                Layout.fillHeight: true

                legend.visible: false
                animationOptions: ChartView.NoAnimation
                antialiasing: antialias
                theme: ChartView.ChartThemeDark

                ValueAxis {
                    id: loopPressureVolumeAxisPressure
                    objectName: "loopPressureVolumeAxisPressure"
                    titleText: "Pressure [mBar]"
                    labelFormat: "%.1f"
                    min: 0
                    max: 30
                }
                ValueAxis {
                    id: loopPressureVolumeAxisVolume
                    objectName: "loopPressureVolumeAxisVolume"
                    titleText: "Volume [mL]"
                    labelFormat: "%.1f"
                    min: 0
                    max: 300
                }
                LineSeries {
                    objectName: "loopPressureVolume"
                    axisX: loopPressureVolumeAxisPressure
                    axisY: loopPressureVolumeAxisVolume
                    useOpenGL: openGL
                }
            }
            ChartView {
                id: loopFlowVolumeChart
                objectName: "loopFlowVolumeChart"
                Layout.fillWidth: true
                Layout.fillHeight: true

                legend.visible: false
                animationOptions: ChartView.NoAnimation
                antialiasing: antialias
                theme: ChartView.ChartThemeDark

                ValueAxis {
                    id: loopFlowVolumeAxisVolume
                    objectName: "loopFlowVolumeAxisVolume"
                    titleText: "Volume [mL]"
                    labelFormat: "%.1f"
                    min: 0
                    max: 300
                }
                ValueAxis {
                    id: loopFlowVolumeAxisFlow
                    objectName: "loopFlowVolumeAxisFlow"
                    titleText: "Flow [mL/min]"
                    labelFormat: "%.1f"
                    min: -1000
                    max: 1500
                }

                LineSeries {
                    objectName: "loopFlowVolume"
                    axisX: loopFlowVolumeAxisVolume
                    axisY: loopFlowVolumeAxisFlow
                    useOpenGL: openGL
                }
            }
            ChartView {
                id: loopPressureFlowChart
                objectName: "loopPressureFlowChart"
                Layout.fillWidth: true
                Layout.fillHeight: true

                legend.visible: false
                animationOptions: ChartView.NoAnimation
                antialiasing: antialias
                theme: ChartView.ChartThemeDark

                ValueAxis {
                    id: loopPressureFlowAxisPressure
                    objectName: "loopPressureFlowAxisPressure"
                    titleText: "Pressure [mBar]"
                    labelFormat: "%.1f"
                    min: 0
                    max: 30
                }
                ValueAxis {
                    id: loopPressureFlowAxisFlow
                    objectName: "loopPressureFlowAxisFlow"
                    titleText: "Flow [mL/min]"
                    labelFormat: "%.1f"
                    min: -1000
                    max: 1500
                }

                LineSeries {
                    objectName: "loopPressureFlow"
                    axisX: loopPressureFlowAxisPressure
                    axisY: loopPressureFlowAxisFlow
                    useOpenGL: openGL
                }
            }
        }
    }
}
