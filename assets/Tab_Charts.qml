import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtCharts 2.3

Item {
    property bool antialias: true
    property bool openGL: false
    property int axisTimeMin : -15

    Rectangle {
        color: Style.btnDnColor
        anchors.fill: parent
    }
    RowLayout {
        anchors.fill: parent

        ColumnLayout {
            id: charts
            spacing: -20
            //https://www.qt.io/blog/2019/06/14/qt-support-aligning-chart-views-underneath
            property bool aligningPlots: true
            property var chartViews: [pressureChart, flowChart, volumeChart]
            function resetPlotAreas() {
                aligningPlot = true
                for (var i = 0; i < chartViews.length; i++) {
                    chartViews[i].plotArea = Qt.rect(0, 0, 0, 0)
                }
                aligningPlot = false
            }
            function alignPlotLeftMargin(chart) {
                if (aligningPlots)
                    return
                aligningPlots = true

                var tmpChart
                var bestRect = chart.plotArea
                var bestChart = chart
                for (var i = 0; i < chartViews.length; i++) {
                    tmpChart = chartViews[i]
                    if (!tmpChart.visible) {
                        continue
                    }
                    if (tmpChart.plotArea.left >
                        Math.ceil(bestRect.left) ||
                        (Math.ceil(tmpChart.plotArea.left) ===
                         Math.ceil(bestRect.left) &&
                         Math.floor(tmpChart.plotArea.right) <
                         Math.floor(bestRect.right))) {
                             bestChart = tmpChart
                             bestRect = tmpChart.plotArea
                     }
                }
                bestRect.left = Math.ceil(bestRect.left)
                bestRect.right = Math.floor(bestRect.right)
                for (i = 0; i < chartViews.length; i++) {
                    tmpChart = chartViews[i]
                    if (!tmpChart.visible) {
                        continue
                    }
                    if (tmpChart !== bestChart) {
                        var newLeft = 20 + bestRect.left -
                              Math.floor(tmpChart.plotArea.left);
                        var newRight = 20 +
                              Math.ceil(tmpChart.plotArea.right) -
                              bestRect.right
                        tmpChart.margins.left = newLeft
                        tmpChart.margins.right = newRight
                    }
                }
                aligningPlots = false
            }
            ChartView {
                id: pressureChart
                objectName: "pressureChart" // for C++
                title: "Pressure"
                Layout.fillWidth: true
                Layout.fillHeight: true

                legend.visible: false
                animationOptions: ChartView.NoAnimation
                antialiasing: antialias
                theme: ChartView.ChartThemeDark

                ValueAxis {
                    id: pressureAxisTime
                    min: axisTimeMin
                    max: 0
                    tickCount: Math.abs(max-min) / 5 + 1
                    labelFormat: "%.1f"
                }
                ValueAxis {
                    id: pressureAxisCmH2O
                    objectName: "pressureAxisCmH2O" // for C++
                    titleText: "cmH<sub>2</sub>0"
                    labelFormat: "%.1f"
                    visible: false //TODO: only show an axis if there are series attached to it
                    min: 0
                    max: 30
                }
                ValueAxis {
                    id: pressureAxismBar
                    objectName: "pressureAxismBar" // for C++
                    titleText: "mBar"
                    labelFormat: "%.1f"
                    min: 0
                    max: 30
                }
                LineSeries {
                    objectName: "pressure_air_supply"
                    name: "Air (supply)"
                    axisX: pressureAxisTime
                    axisY: pressureAxismBar
                    useOpenGL: openGL
                    color: Style.pressureAirSupplyColor
                    //width: 3
                }
                LineSeries {
                    objectName: "pressure_air_regulated"
                    name: "Air (regulated)"
                    axisX: pressureAxisTime
                    axisY: pressureAxismBar
                    useOpenGL: openGL
                    color: Style.pressureAirRegulatedColor
                    //style: Qt.DotLine
                }
                LineSeries {
                    objectName: "pressure_o2_supply"
                    name: "O2 (supply)"
                    axisX: pressureAxisTime
                    axisY: pressureAxismBar
                    useOpenGL: openGL
                    color: Style.pressureO2SupplyColor
                }
                LineSeries {
                    objectName: "pressure_o2_regulated"
                    name: "O2 (regulated)"
                    axisX: pressureAxisTime
                    axisY: pressureAxismBar
                    useOpenGL: openGL
                    color: Style.pressureO2RegulatedColor
                    //style: Qt.DotLine
                }
                LineSeries {
                    objectName: "pressure_buffer"
                    name: "Buffer"
                    axisX: pressureAxisTime
                    axisY: pressureAxismBar
                    useOpenGL: openGL
                    color: Style.pressureBufferColor
                }
                LineSeries {
                    objectName: "pressure_inhale"
                    name: "Inhale"
                    axisX: pressureAxisTime
                    axisYRight: pressureAxisCmH2O
                    useOpenGL: openGL
                    color: Style.pressureInhaleColor
                }
                LineSeries {
                    objectName: "pressure_patient"
                    name: "Patient"
                    axisX: pressureAxisTime
                    axisYRight: pressureAxisCmH2O
                    useOpenGL: openGL
                    color: Style.pressurePatientColor
                }
                onPlotAreaChanged: charts.alignPlotLeftMargin(pressureChart)
                onHeightChanged: pressureChart.plotArea = Qt.rect(0, 0, 0, 0)
            }
            ChartView {
                id: flowChart
                objectName: "flowChart" // for C++
                title: "Flow"
                Layout.fillWidth: true
                Layout.fillHeight: true

                legend.visible: false
                animationOptions: ChartView.NoAnimation
                antialiasing: antialias
                theme: ChartView.ChartThemeDark

                ValueAxis {
                    id: flowAxisTime
                    min: axisTimeMin
                    max: 0
                    tickCount: Math.abs(max-min) / 5 + 1
                    labelFormat: "%.1f"
                }
                ValueAxis {
                    id: flowAxisMlMin
                    titleText: "mL/min"
                    objectName: "flowAxisMlMin" // for C++
                    labelFormat: "%.1f"
                    min: -1000
                    max: 1500
                }
                LineSeries {
                    objectName: "pressure_diff_patient"
                    name: "Patient dP"
                    axisX: flowAxisTime
                    axisY: flowAxisMlMin
                    useOpenGL: openGL
                    color: Style.flowDiffPatientColor
                }
                onPlotAreaChanged: charts.alignPlotLeftMargin(flowChart)
                onHeightChanged: flowChart.plotArea = Qt.rect(0, 0, 0, 0)
            }
            ChartView {
                id: volumeChart
                objectName: "volumeChart" // for C++
                title: "Volume"
                Layout.fillWidth: true
                Layout.fillHeight: true

                legend.visible: false
                animationOptions: ChartView.NoAnimation
                antialiasing: antialias
                theme: ChartView.ChartThemeDark

                ValueAxis {
                    id: volumeAxisTime
                    min: axisTimeMin
                    max: 0
                    tickCount: Math.abs(max-min) / 5 + 1
                    labelFormat: "%.1f"
                }
                ValueAxis {
                    id: volumeAxisMl
                    objectName: "volumeAxisMl" // for C++
                    titleText: "mL"
                    labelFormat: "%.1f"
                    min: 0
                    max: 300
                }
                LineSeries {
                    objectName: "volume"
                    name: "Volume"
                    axisX: volumeAxisTime
                    axisY: volumeAxisMl
                    useOpenGL: openGL
                    color: Style.volumeColor
                }
                onPlotAreaChanged: charts.alignPlotLeftMargin(volumeChart)
                onHeightChanged: volumeChart.plotArea = Qt.rect(0, 0, 0, 0)
            }
            Component.onCompleted: {
                // Alignment is currently extremely bugged when charts are added or removed
                // so for now it's disabled, uncomment the next line once it works better
                //aligningPlots = false
                alignPlotLeftMargin(pressureChart)
                alignPlotLeftMargin(flowChart)
                alignPlotLeftMargin(volumeChart)
            }
        }
        Grid {
            id: grid
            Layout.fillHeight: true
            columns: 2
            spacing: 10
            topPadding: 10

            Repeater {
                model: ListModel {}
                Component.onCompleted: {
                    var i;
                    for (i = 0; i < pressureChart.count; ++i) {
                        model.append({
                            chart: pressureChart,
                            seriesName: pressureChart.series(i).name
                        })
                    }
                    for (i = 0; i < flowChart.count; ++i) {
                        model.append({
                            chart: flowChart,
                            seriesName: flowChart.series(i).name
                        })
                    }
                    for (i = 0; i < volumeChart.count; ++i) {
                        model.append({
                            chart: volumeChart,
                            seriesName: volumeChart.series(i).name
                        })
                    }
                }
                delegate: Rectangle {
                    property QtObject series: chart.series(seriesName)
                    width: applicationWindow.width / 7
                    height: applicationWindow.height / 7
                    border.color: series.color
                    border.width: 3
                    radius: 10
                    color: Style.btnUpColor
                    Timer {
                        interval: 250 // 4 fps
                        running: true
                        repeat: true
                        onTriggered: currentValue.text = series.at(0).y.toFixed(1)
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: checkBox.checked = !checkBox.checked
                    }
                    CheckBox {
                        id: checkBox
                        visible: true
                        text: seriesName
                        font.family: Style.fontFamily
                        font.pointSize: 12
                        font.weight: Font.Bold
                        checked: true
                        onCheckedChanged: {
                            if (series) {
                                //charts.resetPlotAreas()
                                series.visible = checked

                                if (checked) {
                                    chart.visible = true
                                } else {
                                    var vis = false
                                    for (var i = 0; i < chart.count; ++i) {
                                        if (chart.series(i).visible) {
                                            vis = true
                                            break
                                        }
                                    }
                                    chart.visible = vis
                                }
                            }
                        }
                    }
                    Text {
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.bottom: currentValueRect.top
                        text: series.axisYRight ? series.axisYRight.titleText : series.axisY.titleText
                        font.family: Style.fontFamily
                        font.pixelSize: 15
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignTop
                        width: parent.width
                    }
                    Rectangle {
                        id: currentValueRect
                        color: "black"
                        width: parent.width
                        height: applicationWindow.height / 15
                        radius: 10
                        anchors.bottom: parent.bottom
                        Text {
                            id: currentValue
                            text: "-"
                            font.family: Style.fontFamily
                            font.pixelSize: 40
                            anchors.verticalCenter: parent.verticalCenter
                            horizontalAlignment: Text.AlignHCenter
                            anchors.left: parent.left
                            anchors.right: parent.right
                            color: series.color
                        }
                    }
                }
            }
            Rectangle {
                width: applicationWindow.width / 7
                height: applicationWindow.height / 7
                color: "transparent"
                GridLayout {
                    anchors.fill: parent
                    columnSpacing: 10
                    rowSpacing: 10
                    columns: 2
                    RoundButton {
                        text: "60s"
                        font.family: Style.fontFamily
                        font.weight: Font.Bold
                        font.pixelSize: 30
                        radius: 10
                        Layout.fillWidth: true
                        onClicked: axisTimeMin = -60
                    }
                    RoundButton {
                        text: "30s"
                        font.family: Style.fontFamily
                        font.weight: Font.Bold
                        font.pixelSize: 30
                        radius: 10
                        Layout.fillWidth: true
                        onClicked: axisTimeMin = -30
                    }
                    RoundButton {
                        text: "15s"
                        font.family: Style.fontFamily
                        font.weight: Font.Bold
                        font.pixelSize: 30
                        radius: 10
                        Layout.fillWidth: true
                        onClicked: axisTimeMin = -15
                    }
                    RoundButton {
                        text: "5s"
                        font.family: Style.fontFamily
                        font.weight: Font.Bold
                        font.pixelSize: 30
                        radius: 10
                        Layout.fillWidth: true
                        onClicked: axisTimeMin = -5
                    }
                }
            }
        }
    }
}
